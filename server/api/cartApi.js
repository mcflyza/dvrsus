const cartApiRoutes = require('express').Router();
const cartController = require('../controllers/cart');

//Add a item to cart.
cartApiRoutes.post('/add', cartController.addToCart);

//Remove a item from the cart.
// Use request parameter to remove item from cart since you are looking a specific item in cart.
cartApiRoutes.delete('/:id', cartController.removeFromCart);

//Get all users Cart
cartApiRoutes.get('/all', cartController.allCarts);

//Get specific user cart
cartApiRoutes.get('/:id', cartController.specificCart);

cartApiRoutes.delete('/all/:userId', cartController.removeUserCart);

module.exports = cartApiRoutes;
