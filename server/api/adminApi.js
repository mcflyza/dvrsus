const adminApiRoutes = require('express').Router();
const adminController = require('../controllers/admin');

//Gets the admin users.
adminApiRoutes.get('/users', adminController.getAdminUsers);

//When a admin creates a product. No need for request parameter in this case. Since we are inserting data to database.
adminApiRoutes.post('/products', adminController.createProduct);

//When a admin update a current product. Need request parameter since updating a specific product based on  the id.
adminApiRoutes.put('/products/:id', adminController.updateProduct);

//When a admin deletes a product, need an id to specify a product to delete.
adminApiRoutes.delete('/products/:id', adminController.deleteProduct);

//Wrapper Products
adminApiRoutes.post('/wrapper/products', adminController.createProductWrapper);
adminApiRoutes.put('/wrapper/products/:id', adminController.updateProductWrapper);
adminApiRoutes.delete('/wrapper/products/:id', adminController.deleteProductWrapper);

module.exports = adminApiRoutes;
