const wishlistApiRoutes = require('express').Router();
const wishlistController = require('../controllers/wishlist');

//Add a item to wishlist.
wishlistApiRoutes.post('/add', wishlistController.addToWishlist);

//Remove a item from the wishlist.
// Use request parameter to remove item from wishlist since you are looking a specific item in wishlist.
wishlistApiRoutes.delete('/:id', wishlistController.removeFromWishlist);

//Get all users wishlist
wishlistApiRoutes.get('/all', wishlistController.allWishlists);

//Get specific user wishlist
wishlistApiRoutes.get('/:id', wishlistController.specificWishlist);

module.exports = wishlistApiRoutes;
