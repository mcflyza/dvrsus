const productsApiRoutes = require('express').Router();
const productController = require('../controllers/products');

//Getting all the products
productsApiRoutes.get('/products', productController.readAllProducts);

//Getting a specified product
//Use a request parameter, since retrieving a specified product..
productsApiRoutes.get('/products/:id', productController.readProduct);

//Wrapper products
productsApiRoutes.get('/wrapper/products', productController.readAllWrapperedProducts);

productsApiRoutes.get('/wrapper/products/:id', productController.readWrapperedProduct);

//Dewrapped Call
productsApiRoutes.post('/search/product', productController.readDeWrappedProduct);

//Dewrapped Call
productsApiRoutes.post('/search/wrapper/product', productController.readWrappedProduct);

module.exports = productsApiRoutes;
