/*
* API User
*/
const userApiRoutes = require('express').Router();
const userController = require('../controllers/user');

//Read the user's session.
userApiRoutes.get('/data', userController.readUserData);

//Read all user's info.
userApiRoutes.get('/all-users', userController.allUser);

//When user login
userApiRoutes.post('/login', userController.login);

//When the user logouts
userApiRoutes.post('/logout', userController.logout);

//registration
userApiRoutes.post('/register', userController.initNewUser);

//delete user
userApiRoutes.delete('/delete/:id', userController.deleteUser);

module.exports = userApiRoutes;
