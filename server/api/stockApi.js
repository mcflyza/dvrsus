const stockApiRoutes = require('express').Router();
const stockController = require('../controllers/stock');

//Add for the first time new product to the stock
stockApiRoutes.post('/stock/', stockController.initStockForSpecificProduct);

//Getting all products stock
stockApiRoutes.get('/stock', stockController.showStock);

//Getting a specified product
//Use a request parameter, since retrieving a specified product..
stockApiRoutes.get('/stock/:id', stockController.getSpecificStock);

//Update specific product quantity
stockApiRoutes.put('/stock/:id', stockController.updateProductStock);

//Delete specific product from stock
stockApiRoutes.delete('/stock/:id', stockController.deleteProductFromStock);

module.exports = stockApiRoutes;
