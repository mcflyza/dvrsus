//Use stripe for payments
const stripe = require('stripe')('sk_test_gPS7FbpwPjIyUdICGJtjRQz40082FcKuW8');
//const stripe = require('stripe')('sk_live_Ltyxdg99soXlaEIZDYrrgbqa00lCiOMweG');
const stripeApi = require('express').Router();
const uuid = require('uuid/v4');

//pay
const charge = (token, amt, description) => {
  return stripe.charges.create({
    amount: amt * 100,
    currency: 'eur',
    description: description,
    source: token.id
  });
}

stripeApi.post('/pay', async (req, res) => {
  try {
    let  { token, amount, description } = req.body;
    let data = await charge(token, amount, description);

    if(data.status === 'succeeded') {
      res.send('Charged!');
    }
    else {
      res.send('Not charged!');
    }
  } catch (e) {
    console.log(e);
    res.status(500);
  }
});

module.exports = stripeApi;
