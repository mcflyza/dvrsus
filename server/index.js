//Enables the .env file, therefore add a env property to process object.
//Recommend to require it at the top of the file
require('dotenv').config();

//use passport for user account payload
const passport = require('passport');

//Middlewares
//Configure body-parser so you can retrieve values from the req.body, if not the req.body will be undefined.
const bodyParser = require('body-parser');

//Import your express server
const express = require('express');
//Set instance of the express server to a variable
const app = express();

//Require the session for saving user data and giving a user a unique experience.
const session = require('express-session');

//Save session on redis store
const RedisStore = require('connect-redis')(session)
const redis = require('redis').createClient(process.env.REDIS_URL);

//Use cors for enable cross origin requests
const cors = require('cors');

//API handlers
const userApiRoutes = require('./api/userApi');
const productsApiRoutes = require('./api/productsApi');
const stockApiRoutes = require('./api/stockApi');
const adminApiRoutes = require('./api/adminApi');
const cartApiRoutes = require('./api/cartApi');
const wishlistApiRoutes = require('./api/wishlistApi');
const stripeApi = require('./api/stripeApi');
    
//Import your mongoose module to connect to your mongodb database instance using it's connection string.
const mongoose = require('mongoose');

//Define the Port your will be running your server on.
//NOTE: Make sure the Port is the same as the proxy.
const PORT = process.env.PORT || 5000;

const path = require('path');
const pathStaticProd = path.join(__dirname, '../client/build');

// Passport middleware
app.use(passport.initialize());
// Passport config
require('./config/passport')(passport);

//Connect the mongoose to the database using it's connect method.
mongoose.connect( 
    process.env.DVRSUS_MONGO_CONNECTION_STRING, 
    { useNewUrlParser: true }, 
    (err) => {
        let MONGO_CONNECT_SUCCESS = 'Connected to database';
        let MONGO_CONNECT_FAILED = 'Database Error---------------- \n' + err;

        console.log((!err) ? MONGO_CONNECT_SUCCESS : MONGO_CONNECT_FAILED);
    }
);

//Middleware 
//For initializing the req.body. If the middleware is not used, the req.body is undefined.
// Bodyparser middleware
app.use(
    bodyParser.urlencoded({
      extended: false
    })
);
app.use(bodyParser.json());


const redisOpts = {
    client: redis
};

//For storing cookies for the user.
let storingUsersCookies = session({
    store: new RedisStore(redisOpts),
    secret: process.env.SESSION_SECRET,
    //this for resaving the cookie false, if true can cause a memory leak.
    resave: false,
    //saveUnitialized best false, unless connect to a database.
    saveUninitialized: false,
    cookie: {
        //The max age of the cookie
        maxAge: 1000 * 60 * 60 * 24 * 14
    }
});
app.use(storingUsersCookies);

//Allow cross origin requests.
app.use(cors());

//use product api routes
app.use('/api', productsApiRoutes);

//use stock api routes
app.use('/api', stockApiRoutes);

//use admin api routes
app.use('/api', adminApiRoutes);

//use user api routes
app.use('/api/user', userApiRoutes);

//use cart api routes
app.use('/api/user-data/cart', cartApiRoutes);

//use wishlist api routes
app.use('/api/user-data/wishlist', wishlistApiRoutes);

//use stripe api routes
app.use('/api', stripeApi);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(pathStaticProd));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept'
    );
    next();
});

app.use((err, req, res, next) => {
    console.log(err);
    next();
});

app.get('/', (req, res, next) => res.sendFile(path.join(__dirname, '..', 'client', 'build', 'index.html')));

app.use('*', (req, res, next) => res.sendFile(path.join(__dirname, '..', 'client', 'build', 'index.html')));

//start server
app.listen(PORT, () => console.log(`>>> 🌎 magic happens on: http://localhost:${PORT}`));
