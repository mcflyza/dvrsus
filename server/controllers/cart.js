const Cart = require('../models/cart');

module.exports = {
    addToCart(req, res){
        //Destruct the update data from the req.body
        const { userId, productId, quantity } = req.body;

        let newCartRow = new Cart({
            userId, 
            productId, 
            quantity
        });

        //use the .save() to save model to database.
        newCartRow.save();
        
        //Then send back the products.
        res.status(200).json({cartRow: newCartRow});
    },
    allCarts(req, res){
        Cart.find({}).exec((err, products) => {
            let ERROR_MEX = `Get Cart Mongoose Error------------------ \n${err}`;

            err && console.log(ERROR_MEX);

            res.status(200).send(products);
        });
    },
    removeFromCart(req, res) {
        //Get id that will delete    
        const { id } = req.params;

        Cart.deleteOne({_id: id}).exec((err, cartRow) => {
            err && console.log('Delete one Error-----------------', err)
            res.status(200).json({cartRow});
        });
    },
    removeUserCart(req, res) {
        //Get id that will delete
        const { userId } = req.params;

        Cart.deleteMany({userId: userId}).exec((err, cartRow) => {
            err && console.log('Delete one Error-----------------', err)
            res.status(200).json({cartRow});
        });
    },
    specificCart(req, res) {
        const { id } = req.params;
        Cart.find({userId: id}).exec((err, CartRow) => {
            err && console.log('Added operation one Error-----------------', err)
            res.status(200).json({CartRow});
        });
    }
}