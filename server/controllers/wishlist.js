const Wishlist = require('../models/wishlist');

module.exports = {
    addToWishlist(req, res){
        //Destruct the update data from the req.body
        const { userId, productId } = req.body;

        let newWishlistRow = new Wishlist({
            userId,
            productId
        });

        //use the .save() to save model to database.
        newWishlistRow.save();
        
        //Then send back the products.
        res.status(200).json({WishlistRow: newWishlistRow});
    },
    allWishlists(req, res){
        Wishlist.find({}).exec((err, products) => {
            let ERROR_MEX = `Get Wishlist Mongoose Error------------------ \n${err}`;
            let SUCCESS_MEX = `Wishlist loaded succesfully`;
      
            console.log( !err ? SUCCESS_MEX : ERROR_MEX);
            res.status(200).send(products);
          });
    },
    removeFromWishlist(req, res) {
        //Get id that will delete    
        const { id } = req.params;

        Wishlist.deleteOne({_id: id}).exec((err, WishlistRow) => {
            err && console.log('Delete one Error-----------------', err)
            res.status(200).json({WishlistRow});
        });
    },
    specificWishlist(req, res) {
        const { id } = req.params;
        Wishlist.find({userId: id}).exec((err, WishlistRow) => {
            err && console.log('Added operation one Error-----------------', err)
            res.status(200).json({WishlistRow});
        });
    }
}