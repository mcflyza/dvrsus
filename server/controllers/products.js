//Make sure to import it outside of module.exports to have it accessible through out file.
const Product = require('../models/product');
const ProductWrapper = require('../models/productWrapper');

module.exports = {
  readAllProducts(req, res) {
    //When using mongoose can use a callback or a use a exec method to catch and respond to errors.
    Product.find({}).exec((err, products) => {
      let ERROR_MEX = `Get Product Mongoose Error------------------ \n${err}`;

      err && console.log(ERROR_MEX);
      res.status(200).send(products);
    });
  },
  readProduct(req, res) {
    //Destruct the id from the endpoint url, to retrieve  a specific product.
    const { id } = req.params;
    //Copy and paste on of the product's id to the url when testing it.
    //Use the findById to get a specific product.
    Product.findById(id).exec((err, product) => {
      let ERROR_MEX = `Get Product Mongoose Error------------------ \n${err}`;

      err && console.log(ERROR_MEX);
      res.status(200).json({product});
    })
  },
  readDeWrappedProduct(req, res) {
    //Destruct the id from the endpoint url, to retrieve  a specific product.
    const { name, color, size } = req.body;
    
    let productToSearch = {
      name: name, 
      color: color, 
      size: size
    };

    Product.find(productToSearch).exec((err, product) => {
      let ERROR_MEX = `Get Product Mongoose Error------------------ \n${err}`;

      err && console.log(ERROR_MEX);
      res.status(200).json({product});
    })
  },
  readWrappedProduct(req, res) {
    //Destruct the id from the endpoint url, to retrieve  a specific product.
    const { name, color } = req.body;
    
    let wrapperProductToSearch = {
      name: name, 
      color: color
    };

    ProductWrapper.find(wrapperProductToSearch).exec((err, product) => {
      let ERROR_MEX = `Get Product Mongoose Error------------------ \n${err}`;

      err && console.log(ERROR_MEX);
      res.status(200).json({product});
    })
  },
  readAllWrapperedProducts(req, res) {
    //When using mongoose can use a callback or a use a exec method to catch and respond to errors.
    ProductWrapper.find({}).exec((err, products) => {
      let ERROR_MEX = `Get Product Mongoose Error------------------ \n${err}`;

      err && console.log(ERROR_MEX);
      res.status(200).send(products);
    });
  },
  readWrapperedProduct(req, res) {
    //Destruct the id from the endpoint url, to retrieve  a specific product.
    const { id } = req.params;
    //Copy and paste on of the product's id to the url when testing it.
    //Use the findById to get a specific product.
    ProductWrapper.findById(id).exec((err, product) => {
      let ERROR_MEX = `Get Product Mongoose Error------------------ \n${err}`;

      err && console.log(ERROR_MEX);
      res.status(200).json({product});
    })
  }
}