const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const axios = require('axios');

const User = require('../models/user');

module.exports = {
    readUserData(req, res) {
      //Get the session, for update the reducer.
      res.status(200).json({user: req.sessionID});  
    },
    login(req, res) {
        const { email, psw } = req.body;

        User.findOne({email}).then(user => {
            // check if user exists
            if (!user) {
                return res.status(404).json({ emailnotfound: "Email not found" });
            }

            // Check password
            bcrypt.compare(psw, user.psw).then(isMatch => {
                if (isMatch) {
                    // Create JWT Payload
                    const payload = {
                        id: user.id,
                        email: user.email
                    };
                    // Sign token
                    jwt.sign(
                        payload,
                        process.env.SESSION_SECRET,
                        {
                            expiresIn: 31556926 // 1 year in seconds
                        },
                        (err, token) => {
                            res.json({
                                success: true,
                                token: "Bearer " + token
                            });
                        }
                    );
                } else {
                    return res
                    .status(400)
                    .json({ passwordincorrect: "Password incorrect" });
                }
            });
        });
    },
    logout(req, res) {
        //Destroy the session, which logout the user, since when the user session is undefined the redux also logout's
        // the user in the frontend.
        req.session.destroy();
        //Send a message informing  a user successfully logged out.
        res.status(200).json({message: 'Logout Successfully!'});
    },
    initNewUser(req, res) {    
        const { firstname, lastname, email, psw, shippingAddressId, isAdmin } = req.body;

        User.findOne({email}).then(user => {
            //check if email already exist.
            if (user) {
                return res.status(400).json({ email: "Email already exists" });
            } 
            
            //create new user
            const newUser = new User({
                firstname: firstname,
                lastname: lastname, 
                email: email, 
                psw: psw, 
                shippingAddressId: !shippingAddressId && "",
                isAdmin: !isAdmin && false
            });
    
            // hash password before saving in database
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.psw, salt, (err, hash) => {
                    err && console.log(err);

                    newUser.psw = hash;

                    newUser
                        .save()
                        .then(user => res.status(200).json(user))
                        .catch(err => console.log(err));
                });
            });
        });
    },
    deleteUser(req, res) {
        //Destruct the id from the request params, since you have to delete a specific product.
        const { id } = req.params;

        User.deleteOne({_id: id}).exec((err, user) => {
            err && console.log('Delete one Error-----------------', err)
            res.status(200).json({user});
        });
    },
    allUser(req, res){
        User.find().exec((err, users) => {
            let ERROR_MEX = `Get User Mongoose Error------------------ \n${err}`;
            let SUCCESS_MEX = 'User loaded succesfully'
      
            console.log( !err ? SUCCESS_MEX : ERROR_MEX);
            res.status(200).send(users);
        });
    }
}