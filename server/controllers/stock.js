//Make sure to import it outside of module.exports to have it accessible through out file.
const Stock = require('../models/stock');

module.exports = {
    showStock(req, res) {
        Stock.find({}).exec((err, stocks) => {
        let ERROR_MEX = `Get Stock Mongoose Error------------------ \n${err}`;
        let SUCCESS_MEX = `stock loaded succesfully`

        console.log(!err ? SUCCESS_MEX : ERROR_MEX);
        res.status(200).send(stocks);
        });
    },
    getSpecificStock(req, res) {
        const { id } = req.params;

        Stock.find({productId: id}).exec((err, stock) => {
        let ERROR_MEX = `Get Single Stock Error--------------- \n${err}`;
        let SUCCESS_MEX = `stock loaded succesfully`

        console.log(!err ? SUCCESS_MEX : ERROR_MEX);
        res.status(200).json({stock});
        })
    },
    initStockForSpecificProduct(req, res) {
        //Destruct the update data from the req.body
        const { productId, quantity } = req.body;

        let newStock = new Stock({ 
            productId, 
            quantity 
        });

        //use the .save() to save model to database.
        newStock.save();
        
        //Then send back the products.
        res.status(200).json({stock: newStock});
    }, 
    updateProductStock(req, res) {
        //get id 
        const { id } = req.params;

        //Destruct the update data from the req.body
        const { newQuantity } = req.body;
        //Find the product, and update it's properties
        Stock.findOne({productId: id}).exec((err, stock) => {
            stock.quantity = newQuantity;
            
            //Save the product with updated data.
            stock.save();
            
            //Then send back the data, just for testing purposes.
            res.status(200).json({stock});
        })
    }, 
    deleteProductFromStock(req, res) {
        //Destruct the id from the request params, since you have to delete a specific product.
        const { id } = req.params;

        //Use an object to delete the specified product.
        Stock.deleteOne({productId: id}).exec((err, stock) => {
            if(err) console.log('Delete One Error-----------------', err)
            res.status(200).json({stock});
        });
    }
}