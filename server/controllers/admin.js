const Product = require('../models/product');
const User = require('../models/user');
const ProductWrapper = require('../models/productWrapper');

module.exports = {
    getAdminUsers(req, res) {
        User.find().exec((err, users) => {
            if(err) console.log('Find Admin Users Error---------------', err);
            res.status(200).json({users});
        })
    }, 
    createProduct(req, res) {
        //Destruct the update data from the req.body
        const { name, description, price, color, size, pictures } = req.body;

        let newProduct = new Product({
            name,
            description,
            price,
            color, 
            size,
            pictures
        });

        //use the .save() to save model to database.
        newProduct.save();
        
        //Then send back the products.
        res.status(200).json({product: newProduct});
    }, 
    updateProduct(req, res) {
        //get id 
        const { id } = req.params;

        //Destruct the update data from the req.body
        const { name, description, price, color, size, pictures } = req.body;
        //Find the product, and update it's properties
        Product.findById(id).exec((err, product) => {
            err && console.log('Updated Product-----------------', err);

            product.name = name;
            product.description = description;
            product.price = price;
            product.color = color;
            product.size = size;         
            product.pictures = pictures;

            //Save the product with updated data.
            product.save();
            
            //Then send back the data, just for testing purposes.
            res.status(200).json({product});
        })
    }, 
    deleteProduct(req, res) {
        //Destruct the id from the request params, since you have to delete a specific product.
        const { id } = req.params;

        //Use an object to delete the specified product.
        Product.deleteOne({_id: id}).exec((err, product) => {
            if(err) console.log('Delete One Error-----------------', err)
            res.status(200).json({product});
        });
    },
    createProductWrapper(req, res) {
        //Destruct the update data from the req.body
        const { name, description, price, color, size, pictures } = req.body;

        let newProductWrapper = new ProductWrapper({
            name,
            description,
            price,
            color, 
            size,
            pictures
        });

        //use the .save() to save model to database.
        newProductWrapper.save();
        
        //Then send back the products.
        res.status(200).json({product: newProductWrapper});
    }, 
    updateProductWrapper(req, res) {
        //get id 
        const { id } = req.params;

        //Destruct the update data from the req.body
        const { name, description, price, color, size, pictures } = req.body;
        //Find the product, and update it's properties
        ProductWrapper.findById(id).exec((err, product) => {
            err && console.log('Updated Product-----------------', err);

            product.name = name;
            product.description = description;
            product.price = price;
            product.color = color;
            product.size = size;         
            product.pictures = pictures;

            //Save the product with updated data.
            product.save();
            
            //Then send back the data, just for testing purposes.
            res.status(200).json({product});
        })
    }, 
    deleteProductWrapper(req, res) {
        //Destruct the id from the request params, since you have to delete a specific product.
        const { id } = req.params;

        //Use an object to delete the specified product.
        ProductWrapper.deleteOne({_id: id}).exec((err, product) => {
            if(err) console.log('Delete One Error-----------------', err)
            res.status(200).json({product});
        });
    }
}