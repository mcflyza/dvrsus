const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cart = new Schema({
    userId: String,
    productId: String,
    quantity: String
});

module.exports = mongoose.model('Cart', cart);