const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orders = new Schema({
    userId: String,
    productId: String,
    quantity: Number,
    date: Date
});

module.exports = mongoose.model('Orders', orders);