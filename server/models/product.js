const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const product = new Schema({
    name: String,
    description: String,
    price: Number,
    color: String,
    size: String,
    pictures: {
        type: Map,
        of: String
    }
});

module.exports = mongoose.model('Product', product);