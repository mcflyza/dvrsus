const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const shippingAddress = new Schema({
    userId: String,
    province: String,
    city: Number,
    street: String,
    civicNumber: String,
    CAP: String
});

module.exports = mongoose.model('ShippingAddress', shippingAddress);