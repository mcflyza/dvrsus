const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const wishlist = new Schema({
    userId: String,
    productId: String
});

module.exports = mongoose.model('Wishlist', wishlist);