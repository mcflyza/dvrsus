const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
    shippingAddressId: String,
    firstname: String,
    lastname: String,
    email: String,
    psw: String,
    isAdmin: Boolean
});

module.exports = mongoose.model('User', user);
