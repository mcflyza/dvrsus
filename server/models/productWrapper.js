const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productWrapper = new Schema({
    name: String,
    description: String,
    price: Number,
    color: String,
    size: {
        type: Map,
        of: String
    },
    pictures: {
        type: Map,
        of: String
    }
});

module.exports = mongoose.model('ProductWrapper', productWrapper);