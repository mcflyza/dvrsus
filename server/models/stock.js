const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const stock = new Schema({
    productId: String,
    quantity: Number
});

module.exports = mongoose.model('Stock', stock);