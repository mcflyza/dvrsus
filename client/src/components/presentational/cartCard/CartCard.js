import React, {Component} from 'react';
import './cartCard.css';

import AddToWishlist from '../../container/shared/cta/addToWishlist/AddToWishlist';
import RemoveFromCart from '../../container/shared/cta/removeFromCart/RemoveFromCart';

class CartCard extends Component {

    constructor(props) {
        super();
        
        let {_id, cartRow, size, name, color, price, pictures} = props.item;

        this.state = {
            id: _id,
            name: name, 
            color: color,
            size: size,
            price: price,
            pictures: pictures,
            cartRow: cartRow
        }
    }

    render() {
        let {id, size, cartRow, name, price, color, pictures} = this.state;

        let pictureSrc = Object.values(pictures)[0];

        return (
            <div className='cartCardWrapper product-cart container'>
                <article className='cartArticle'>
                    <header className='cartHeader'>
                        <h2 className='cartTitle'>
                            <div class='imgWrapper'>
                                <img alt='cartImage' src={pictureSrc} />
                            </div>
                        </h2>
                    </header>
                    <div className='cartInfoProductWrapper'>
                        <main>
                            <div className='cartInfoProduct'>
                                <section>
                                    <h2 className='product-cart label titleSection'>{name}</h2>
                                    <p className='product-cart label size'>{size}</p>
                                    <p className='product-cart label color'>{color}</p>
                                    <p className='product-cart label prize'>€{price},00</p>
                                </section>
                            </div>
                        </main>
                        <footer>
                            <AddToWishlist productId={id}/>
                            <RemoveFromCart cartRowId={cartRow} />
                        </footer>
                    </div>
                </article>
            </div>
        )
    }
};

export default CartCard;