import React, {Component} from 'react';
import './wishlistCard.css';

import RemoveFromWishlist from '../../container/shared/cta/removeFromWishlist/RemoveFromWishlist';
import AddToCart from '../../container/shared/cta/addToCart/AddToCart';

class WishlistCard extends Component {

    constructor(props) {
        super();
        
        let {_id, wishlistRow, size, name, color, price, pictures} = props.item;

        this.state = {
            id: _id,
            name: name, 
            color: color,
            size: size,
            price: price,
            pictures: pictures,
            wishlistRow: wishlistRow
        }
    }

    render() {
        let {id, size, wishlistRow, name, price, color, pictures} = this.state;

        let pictureSrc = Object.values(pictures)[0];

        return (
            <div className='wishlistCardWrapper product-card container'>
                <article className='whislistArticle'>
                    <header className='wishlistHeader'>
                        <h2 className='whislistTitle'>
                            <div class='imgWrapper'>
                                <img alt='cartImage' src={pictureSrc} />
                            </div>
                        </h2>
                    </header>
                    <div className='wishlistInfoProductWrapper'>
                        <main>
                            <div className='wishlistInfoProduct'>
                                <section>
                                    <h2 className='product-card label titleSection'>{name}</h2>
                                    <p className='product-card label size'>{size}</p>
                                    <p className='product-card label color'>{color}</p>
                                    <p className='product-card label prize'>€{price},00</p>
                                </section>
                            </div>
                        </main>
                        <footer>
                            <AddToCart productId={id}/>
                            <RemoveFromWishlist wishRowId={wishlistRow} />
                        </footer>
                    </div>
                </article>
            </div>
        )
    }
};

export default WishlistCard;