import React from 'react';

const Icon = (props) => (
  <div className='icon'>
    <img alt='icon' src={props.srcIcon}></img>
  </div>
);
  
export default Icon;