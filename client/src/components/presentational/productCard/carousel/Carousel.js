import React, {Component} from 'react';
import Slider from "react-slick";
import './carousel.css';

class Carousel extends Component {

    constructor() {
        super();
        
        let imgsList = [];
        console.log(this.props)
        this.props && this.props.imgs.map(img => imgsList.push(
                <div className='sliderImgWrapper' key={this.props.name + img}>
                    <img className='sliderImg' alt={this.props.name + img} src={img}></img>
                </div>
            )
        );

        this.state = {
            imgsList: imgsList
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        let imgsList = [];
        nextProps.imgs.map(img => imgsList.push(
                <div className='sliderImgWrapper' key={nextProps.name + img}>
                    <img className='sliderImg' alt={nextProps.name + img} src={img}></img>
                </div>
            )
        );

        return {imgsList: imgsList};
    }

    render() {

        let sliderSettings = {
            dots: false
        };

        return (
            <div className='carouselWrapper'>            
                <link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
                <Slider {...sliderSettings}>
                    {this.state.imgsList}
                </Slider>
            </div>
        );
    }
};

export default Carousel;