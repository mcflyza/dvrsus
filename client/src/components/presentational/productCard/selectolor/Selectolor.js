import React, {Component} from 'react';
import './selectolor.css';

class Selectolor extends Component {

    constructor(props) {
        super();
        this.state = {
            itemColor: props.color,
            firstcolor: 'black',
            secondcolor: props.name && props.name.includes('Jeans') ? 'blue' : 'white'
        }

        this.takeSelectedOption = this.takeSelectedOption.bind(this);
    }

    takeSelectedOption = (evt) => {
        this.setState({ selectedOption: evt.target.value },
        () => {
            this.props.userColorSelect();
        })
    }

    render() {
        let { firstcolor, secondcolor } = this.state;
        let colorSelected = this.state.selectedOption;

        return (
            <div className='selectolorWrapper' onChange={this.takeSelectedOption} data-selected={this.state.selectedOption}>
                <div className='radio firstcolor'>
                    <input type='radio' value={firstcolor} 
                        checked={colorSelected === firstcolor} 
                        style={{backgroundColor: firstcolor}}
                        onClick={this.takeSelectedOption}
                    />
                </div>
                <div className='radio secondcolor'>
                    <input type='radio' value={secondcolor} 
                        checked={colorSelected === secondcolor} 
                        style={{backgroundColor: secondcolor}}
                        onClick={this.takeSelectedOption}
                        disabled={secondcolor === 'blue' ? true : false}
                    />
                </div>
            </div>
        )
    }
};

export default Selectolor;