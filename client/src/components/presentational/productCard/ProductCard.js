import React, {Component} from 'react';
import axios from 'axios';
import './productCard.css';

import Carousel from './carousel/Carousel';
import AddToCart from '../../container/shared/cta/addToCart/AddToCart';
import AddToWishlist from '../../container/shared/cta/addToWishlist/AddToWishlist';
import Selectize from './selectize/Selectize';
import Selectolor from './selectolor/Selectolor';

class ProductCard extends Component {

    constructor(props) {
        super();
        let { _id, name, description, price, color, pictures, size} = props.item;

        this.state = {
            _id: _id,
            name: name, 
            description: description,
            price: price,
            color: color,
            size: size,
            pictures: pictures,
            productId: '',
            imagesPathToPass: [],
            invalidSelection: true
        }

        this.getSize = this.getSize.bind(this);
        this.getColor = this.getColor.bind(this);
        this.updateImage = this.updateImage.bind(this);
        this.userColorSelect = this.userColorSelect.bind(this);
        this.userActionClick = this.userActionClick.bind(this);
    }

    componentDidMount() {
        this.updateImage();
        window.addEventListener('resize', this.updateImage.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateImage.bind(this));
    }

    updateImage() {
        let allImgs = this.state.pictures && Object.values(this.state.pictures);

        if(allImgs) {
            let mobileImgs = allImgs.filter(img => img.includes('smartphone'));
            let desktopImgs = allImgs.filter(img => img.includes('desktop'));
    
            let imagesPathToPass = window.innerWidth > 1025 ? desktopImgs : mobileImgs
    
            this.setState({imagesPathToPass});
        }
    }

    getSize() {
        let sizeSelect = document.querySelector('.selectizeWrapper select');
        let size = sizeSelect && sizeSelect.getAttribute('data-selected');

        return size ? size : '';
    }

    getColor() {
        let colorSelect = document.querySelector('.selectolorWrapper');
        let color = colorSelect && colorSelect.getAttribute('data-selected');

        return color ? color : '';
    }

    userColorSelect() {
        let wrapperProductToSearch = {
            name: this.state.name,
            color: this.getColor().toUpperCase()
        };

        axios
            .post('/api/search/wrapper/product', wrapperProductToSearch)
            .then(res => {
                    let { pictures, color } = res.data.product[0];
                    
                    this.setState({
                        pictures: pictures,
                        color: color
                    }, () => {
                        this.updateImage()
                    })
                }
            )
            .catch(err => console.error('[PDP] getExactWrapperdedProduct ', err));
    }

    userActionClick() {
        let size = this.getSize().toUpperCase();
        let color = this.getColor().toUpperCase();
        
        if(size === '' || color === '') {
            return '';
        } else {
            this.setState({
                invalidSelection: false
            });

            let productToSearch = {
                name: this.state.name,
                color: color,
                size: size
            };

            axios
                .post('/api/search/product', productToSearch)
                .then(res => 
                    this.setState({
                        productId: res.data.product[0]._id
                    })
                )
                .catch(err => console.error('[PDP] getExactProduct ', err));
        }
    }

    render() {
        let { color, name, description, price, size, imagesPathToPass} = this.state;

        return (
            <div className='productCardWrapper product-card container'>
                <Carousel name={name} imgs={imagesPathToPass} />
                <div className='infoSelectionWrapper'>
                    <h2 className='title-card label'>{name}</h2>
                    <span className='modelSizeDescription'>La taglia nella foto e' una: M | Altezza modello: 185cm</span>
                    <hr />
                    <span className='prize-card label'>€{price},00</span><span className='iva-card label'> IVA inclusa</span>
                    <div className='chooseParamWrapper'>
                        <Selectolor userColorSelect={this.userColorSelect} color={color} name={name}/>
                        <Selectize execOnClick={this.userActionClick} options={size}/>
                    </div>
                    <div className='userAction' onClick={this.userActionClick}>
                        <AddToCart productId={this.state.productId} error={this.state.invalidSelection}/>
                        <AddToWishlist productId={this.state.productId} error={this.state.invalidSelection}/>
                    </div>
                    <section className='descriptionWrapper product-card label'>
                        <h2>
                            Descrizione
                        </h2>
                        <p>
                            {description}
                        </p>
                    </section>
                </div>
            </div>
        )
    }
};

export default ProductCard;