import React, {Component} from 'react';
import './selectize.css';

class Selectize extends Component {

    constructor(props) {
        super();
        this.state = {
            options: props.options
        }

        this.takeSelectedOption = this.takeSelectedOption.bind(this);
    }

    takeSelectedOption = (evt) => {
        this.setState({ selected: evt.target.value },
            () => {
                this.props.execOnClick();
            });
    }

    render() {
        let options = this.state.options && Object.values(this.state.options);
        let DOMoptions = [
            <option key='first' value='null'>Seleziona la taglia</option>
        ];

        options && options.map( option => DOMoptions.push(
                <option key={option} value={option}>{option}</option>
            )
        );

        return (
            <div className='selectizeWrapper'>
                <select className='selectize' onChange={this.takeSelectedOption} data-selected={this.state.selected}>
                    {DOMoptions}
                </select>
            </div>
        )
    }
};

export default Selectize;