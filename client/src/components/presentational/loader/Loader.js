import React from 'react';
import './loader.css';
import BackToShop from '../backToShop/backToShop';

const Loader = () => {
    return (
        <div className="loader-div">
            <h3>Non hai selezionato nessun prodotto</h3>
            <BackToShop />
        </div>
    );
};

export default Loader;