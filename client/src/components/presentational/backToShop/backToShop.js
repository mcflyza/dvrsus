import React from 'react';
import { Link } from 'react-router-dom';

import './backToShop.css';

const BackToShop = () => {
    return (
        <div className='backToShopLink'>
            <Link to='/'>Torna allo shop</Link>
        </div>
    );
};

export default BackToShop;