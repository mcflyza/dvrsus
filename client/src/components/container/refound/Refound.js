import React, { Component } from 'react';
import './refound.css';
  
class Refound extends Component {
    render() {
        return (
            <div className='wrapperRefound'>
                <article>
                    <header>
                        <h1>
                            RESI E RIMBORSI
                        </h1>
                    </header>
                    <main>
                        <div>
                            Se il tuo ordine non ti soddisfa puoi restituire i prodotti purché nuovi e non utilizzati, con i cartellini originali.
                            La pratica di reso può essere aperta entro e non oltre i 10 giorni dalla data di acquisto.
                            I prodotti restituiti non devono essere stati lavati, indossati o modificati.<br/>

                            Riceverai il tuo rimborso tramite bonifico bancario.<br/>

                            Le spese di spedizione pagate per la consegna dell’ordine non saranno rimborsate.<br/>

                            <br/><strong>COSTI DI RIMBORSO</strong><br/>

                            In Italia e in Europa effettuiamo il reso gratuitamente per ordini superiori a 99€. Al di fuori di queste zone il costo del ritiro è di 30,00 € e verrà detratto dal tuo rimborso.<br/>

                            Per importi inferiori a 99€ il costo del reso è a carico del consumatore.<br/>

                            <br/><strong>TEMPI DI RIMBORSO</strong><br/>

                            Un rimborso viene accreditato entro i 35 giorni lavorativi.<br/>
                            Generalmente i tempi necessari non superano i 5/10 giorni lavorativi dal momento in cui il tuo reso arriva ai nostri magazzini.
                        </div>
                    </main>
                </article>
            </div>
        )
    }
}

export default Refound;