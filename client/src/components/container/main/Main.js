import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './main.css';

import Home from '../home/Home';
import Cart from '../cart/Cart';
import ProductDetailsPage from '../productDetailsPage/ProductDetailsPage';
import Contact from '../contact/Contact';
import Header from '../shared/header/Header';
import Auth from '../shared/auth/Auth';

import PrivateRoute from '../private-route/PrivateRoute';
import Account from '../account/Account';
import Wishlist from '../wishlist/Wishlist';
import Privacy from '../privacy/Privacy';
import Refound from '../refound/Refound';


const Main = () => (
  <main className='wrapperMain'>
    <Header />
    <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/product-details-page' component={ProductDetailsPage} />
        <Route path='/contacts' component={Contact} />
        <Route path='/auth' component={Auth} />
        <Route path='/privacy' component={Privacy} />
        <Route path='/refound' component={Refound} />
        <PrivateRoute path='/cart' component={Cart} />
        <PrivateRoute path='/account' component={Account} />
        <PrivateRoute path='/wishlist' component={Wishlist} />
    </Switch>
  </main>
)

export default Main;