import React, { Component } from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from '../../shared/checkoutForm/CheckoutForm';

class StripeSupplier extends Component {
    constructor() {
        this.state = {
            isProd: true,
            apiKeyProd: 'pk_live_Xnfd58dFm47OFSebf4Hm2Fid00aoIw6W2c',
            apiKeyFake: 'pk_test_wJBcKm0BNXlUzisVYXnUW8es00R2fvk438'
        }
    }

    render() {
        return (
            <div className='stripeSupplierWrapper'>
                <StripeProvider cartDescription={this.props.cartDescription} totalCart={this.props.totalCart} apiKey={this.state.isProd ? this.state.apiKeyProd : this.state.apiKeyFake}>
                    <Elements cartDescription={this.props.cartDescription} totalCart={this.props.totalCart}>
                        <CheckoutForm cartDescription={this.props.cartDescription} totalCart={this.props.totalCart}/>
                    </Elements>
                </StripeProvider>
            </div>
        );
    }
}

export default StripeSupplier;
