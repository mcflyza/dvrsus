import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import axios from 'axios';
import { logoutUser } from '../../../redux/actions/authActions';

import './cart.css'
import Loader from '../../presentational/loader/Loader';
import CartCard from '../../presentational/cartCard/CartCard';
import StripeSupplier from './stripeSupplier/StripeSupplier';
import BackToShop from '../../presentational/backToShop/backToShop';

class Cart extends Component {
    constructor() {
        super();
        this.state = {
            products: [],
            loading: true,
            totalCart: 0, 
            cartDescription: ''
        }
    }

    componentDidMount() {
        const { user } = this.props.auth;

        axios
            .get(`/api/user-data/cart/${user.id}`)
            .then(res => {
                let products = [];
                res.data.CartRow.map(
                    row => 
                        axios
                            .get(`/api/products/${row.productId}`)
                            .then(res => {
                                let product = res.data.product;

                                if(product) {
                                    product.cartRow = row._id;

                                    products.push(product);

                                    let updateDescription = this.state.cartDescription + 
                                        '\nProdotto: ' + 
                                        product.name + 
                                        ' ' +
                                        product.size + 
                                        ' ' +
                                        product.color;

                                    this.setState({
                                        products: products, 
                                        loading: false,
                                        totalCart: this.state.totalCart + product.price,
                                        cartDescription: updateDescription
                                    });
                                }
                            })
                            .catch(err => console.log('Reading product Error-------', err))
                );
            })
            .catch(err => console.log('Read all products Error-------', err));
    }

    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
    };

    render() {
        const {products, loading, totalCart} = this.state;

        const CartProductDetails = () => (
            <div className='cart container'>
                <div className='cart-products container'>
                    {
                        products.length ? 
                            products.map(product => <CartCard item={product}/>) 
                            : null
                    }
                </div>
                <div className='totalCartWrapper'>
                    <span>Total</span>
                    <span>€{totalCart},00</span>
                </div>
                <StripeSupplier 
                    cartDescription={this.state.cartDescription}
                    totalCart={totalCart}
                />
            </div>
        )

        return (
            <div className='cartWrapper'>
                <section className='cartSection'>
                    <div className='cartHeaderWrapper'>
                        <h2>Cart</h2>
                        <BackToShop />
                    </div>
                    { !loading ? <CartProductDetails /> : <Loader /> }
                </section>
            </div>
        );
    }
}

Cart.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Cart);
