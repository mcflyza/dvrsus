import React, { Component } from 'react';
import axios from 'axios';
import './productDetailsPage.css';
  
import ProductCard from '../../presentational/productCard/ProductCard.js'

class ProductDetailsPage extends Component {
	constructor() {
        super();
        this.state = {
			product: '',
			loading: false
        }
    }

    componentDidMount() {
        let { productId } = this.props.location.state;

        axios
            .get(`/api/wrapper/products/${productId}`)
            .then(res => this.setState({
				product: res.data.product,
				loading: true
            }))
            .catch(err => console.log('Reading product Error-------', err));
    }   

    componentWillReceiveProps(nextProps) {
		if (this.state.productId === nextProps.location.state) return;
		
		this.setState({
			loading: false
		});
        
        let { productId } = nextProps.location.state;

        axios
            .get(`/api/wrapper/products/${productId}`)
            .then(res => this.setState({
				product: res.data.product,
				loading: true
            }))
            .catch(err => console.log('Reading product Error-------', err));
    }

    render() {
        const {product, loading} = this.state;

        return <div className='productDetailsPageWrapper'>
            { loading ? <ProductCard item={product}/> : null }
        </div>
    }

}

export default ProductDetailsPage;