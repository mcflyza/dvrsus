import React, { Component } from 'react';
import './contact.css';

import FollowUs from '../shared/footer/followUs/FollowUs';
  
class Contact extends Component {
    render() {
        return (
            <div className='wrapperContact'>
                <article>
                    <header>
                        <h1>
                            Contatti
                        </h1>
                    </header>
                    <main>
                        <section className='contactUs'>
                            <h3>
                                Parla direttamente con noi
                            </h3>
                            <div>
                                <span>Email: info@dvrsus.com</span>
                                <span>Cellulare: 3346317365</span>
                                <FollowUs />
                            </div>
                        </section>
                        <section className='retailStore'>
                            <h3>
                                Compra nei nostri store convenzionati
                            </h3>
                            <div>
                                <ul>
                                    <li><strong>Seventeen street</strong>, Via Guglielmo Marconi 23,Torre Del Greco. 331 812 1312</li>
                                    <li><strong>Kaluma</strong>, Via pittore 31, San Giorgio a Cremano. 331 818 6730</li>
                                    <li><strong>Anders abbigliamento</strong>, Via panoramica 90,Ercolano. 333 491 5665</li>
                                    <li><strong>Esse Lab</strong>, Via Fraustino 23, Volla. 339 148 0923</li>
                                </ul>
                            </div>
                        </section>
                    </main>
                    <footer>
                        Advisory and marketing: info@debugdesign.com
                    </footer>
                </article>
            </div>
        )
    }
}

export default Contact;