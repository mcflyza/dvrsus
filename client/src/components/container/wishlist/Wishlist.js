import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import axios from 'axios';
import './wishlist.css'

import { logoutUser } from '../../../redux/actions/authActions';

import Loader from '../../presentational/loader/Loader'
import WishlistCard from '../../presentational/wishlistCard/WishlistCard';
import BackToShop from '../../presentational/backToShop/backToShop';

class Wishlist extends Component {
    constructor() {
        super();
        this.state ={
            products: [],
            loading: true
        }
    }

    componentDidMount() {
        const { user } = this.props.auth;

        axios
            .get(`/api/user-data/wishlist/${user.id}`)
            .then(res => {
                let products = [];
                res.data.WishlistRow.map(
                    row => 
                        axios
                            .get(`/api/products/${row.productId}`)
                            .then(res => {
                                let product = res.data.product;
                                
                                if(product) { 
                                   product.wishlistRow = row._id;

                                    products.push(product);

                                    this.setState({
                                        products: products, 
                                        loading: false
                                    });
                                }
                            })
                            .catch(err => console.log('Reading product Error-------', err))
                );
            })
            .catch(err => console.log('Read all products Error-------', err));
    }

    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
    };

    render() {
        const products = this.state.products;
        const loading = this.state.loading;

        const WishlistProductDetails = () => (
            <div className='wishlist container'>
                <div className='wishlist-products container'>
                    {
                        products.length ? 
                            products.map(product => <WishlistCard item={product} />) 
                            : null
                    }
                </div>
            </div>
        );

        return (
            <div className='wishlistWrapper'>
                <section className='wishlistSection'>
                    <div className='wishlistHeaderWrapper'>
                        <h2>Wishlist</h2>
                        <BackToShop />
                    </div>
                    { !loading ? <WishlistProductDetails /> : <Loader /> }
                </section>
            </div>
        );
    }
}

Wishlist.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Wishlist);
