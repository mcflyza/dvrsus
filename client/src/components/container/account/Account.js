import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../../redux/actions/authActions';
import './account.css';

import Input from '../shared/input/Input'

class Account extends Component {
    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
    };

    render() {
        const { user } = this.props.auth;

        return (
            <div className='accountWrapper'>
                    <h4>
                        Complimenti, <strong>{user.email ? user.email : ''}</strong><br/>sei loggato nell'E-COMMERCE ufficiale DVRS.US 👏
                    </h4>
                    <div className='wrapperLogout' onClick={this.onLogoutClick}>
                        <Input type='submit' value='ESCI' />
                    </div>
            </div>
        );
    }
}

Account.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Account);
