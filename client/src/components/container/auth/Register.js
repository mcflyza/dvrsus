import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import RegisterForm from '../shared/form/registerForm/RegisterForm'

class Register extends Component {
    componentDidMount() {
        // If logged in and user navigates to Register page, should redirect them to dashboard
        if (this.props.auth.isAuthenticated) {
            this.props.history.push('/account');
        }
    }
      
    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push('/account'); // push user to dashboard when they Register
        }
        
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }
    
    render() {
        return <div className='wrapperRegister'>
            <section className='register'>
                <h2>REGISTRATI</h2>
                <hr/>
                <RegisterForm formVisibility={this.props.formVisibility}/>
            </section>
        </div>
    }
}

Register.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(Register);