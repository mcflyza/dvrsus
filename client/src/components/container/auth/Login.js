import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import LoginForm from '../shared/form/loginForm/LoginForm'

class Login extends Component {
    componentDidMount() {
        // If logged in and user navigates to Login page, should redirect them to dashboard
        if (this.props.auth.isAuthenticated) {
            this.props.history.push('/account');
        }
    }
      
    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push('/account'); // push user to dashboard when they login
        }
        
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }
    
    render() {
        
        return <div className='wrapperLogin'>
            <section className='login'>
                <h2>ACCEDI</h2>
                <hr/>
                <LoginForm formVisibility={this.props.formVisibility}/>
            </section>
        </div>
    }
}

Login.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(Login);