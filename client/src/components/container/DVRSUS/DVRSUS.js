import React from 'react';
import './DVRSUS.css';

import Footer from '../shared/footer/Footer'
import Disclaimer from '../shared/disclaimer/Disclaimer'
import Main from '../main/Main';

import jwt_decode from 'jwt-decode';
import setAuthToken from '../../../utils/setAuthToken';
import { setCurrentUser, logoutUser } from '../../../redux/actions/authActions';

import store from '../../../redux/store';

// Check for token to keep user logged in
if (localStorage.jwtToken) {
	// Set auth token header auth
	const token = localStorage.jwtToken;
	setAuthToken(token);

	// Decode token and get user info and exp
	const decoded = jwt_decode(token);

	// Set user and isAuthenticated
	store.dispatch(setCurrentUser(decoded));

	// Check for expired token
	const currentTime = Date.now() / 1000; // to get in milliseconds
	if (decoded.exp < currentTime) {
		// Logout user
		store.dispatch(logoutUser());
		// Redirect to login
		window.location.href = "./auth";
	}
}

const DVRSUS = () => 
	<div className='wrapperDVRSUS'>
		<Disclaimer />
		<Main />
		<Footer />
	</div>

export default DVRSUS;
