import React, {Component} from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../../../redux/actions/authActions';
import './checkoutForm.css'


class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      complete: false,
      description: this.props.cartDescription,
      firstname: '',
      lastname: '',
      address: '',
      province: '',
      city: ''
    };
    this.submit = this.submit.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {description: nextProps.cartDescription};
  }

  async submit(evt) {
    evt.preventDefault();

    let { token } = await this.props.stripe.createToken({
      name: this.props.auth.user.email
    });

    let  { firstname, lastname, address, province, city, description} = this.state;

    //add address to description
    let addToDescription = '\n\nInviare i prodotti sopra elencati al seguente indirizzo:\nNOME: ' + firstname +
      '\nCOGNOME: ' +
      lastname +
      '\nVIA: ' + 
      address +
      '\nPROVINCIA: ' +
      province + 
      '\nCITTA: ' +
      city;
    
    description = description + addToDescription
    
    //payment request
    let response = await axios
      .post('/api/pay', {
        token: token,
        amount: this.props.totalCart,
        description: description
      });

    if (response.status === 200) {
      //set state completed payment at true
      this.setState({
        complete: true
      });

      //delete user cart
      const { user } = this.props.auth;

      await axios
        .delete(`/api/user-data/cart/all/${user.id}`)
        .then()
        .catch(err => console.log(err));

    } else {
      alert('PAGAMENTO NON RIUSCITO');
    }
  }

  changeFormHandler = evt => {
    let target = evt.target
    let field = target.name
    let value = target.value;
    switch(field) {
      case 'address': 
        this.setState({
          address: value
        })
        break;
      case 'cap':
        this.setState({
          cap: value
        })
        break;
      case 'firstname':
        this.setState({
          firstname: value
        })
        break;
      case 'lastname':
        this.setState({
          lastname: value
        })
        break;
      case 'province':
        this.setState({
          province: value
        })
        break;
      case 'city':
        this.setState({
          city: value
        })
        break;
      default: console.log('formerror');
    }
  }

  render() {
    if (this.state.complete) return <h1>Acquisto riuscito</h1>;

    return (
      <div className='checkoutWrapper'>
        <form className='checkoutForm' onSubmit={this.submit}>
          <input type="text"
            name="firstname"
            placeholder="Nome"
            value={this.state.firstname} 
            onChange={this.changeFormHandler}
            required={true}
          />

          <input type="text"
            name="lastname"
            placeholder="Cognome"
            value={this.state.lastname}
            onChange={this.changeFormHandler}
            required={true}
          />

          <input type="text"
            name="address"
            placeholder="Indirizzo"
            value={this.state.address} 
            onChange={this.changeFormHandler}
            required={true}
          />

          <input type="text"
            name="city"
            placeholder="Citta'"
            value={this.state.city} 
            onChange={this.changeFormHandler}
            required={true} 
          />

          <input type="text"
            name="province"
            placeholder="Provincia"
            value={this.state.province} 
            onChange={this.changeFormHandler}
            required={true} 
          />

          <CardElement required={true}/>
          <button type='submit'>Acquista</button>
        </form>
      </div>
    );
  }
}

CheckoutForm.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(injectStripe(CheckoutForm));