import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './legalArea.css';


class LegalArea extends Component {
    
    onClick = (evt) => evt.preventDefault();
    
    render() {
        return <div className='wrapperLegalArea'>
            <div className='legalAreaLeft'>
                <p>&copy;2019 DVRSUS Italia P.IVA IT09203761219</p>
            </div>
            <div className='legalAreaRight'>
                <Link to='/privacy'>Privacy Policy</Link>  
                <Link to='/refound'>Reso e rimborsi</Link>
            </div>
        </div>
    }
}

export default LegalArea;
