import React, { Component } from 'react';
import './paymentMethods.css';

import Icon from '../../../../presentational/icon/Icon';
import paymentMethodsImage from '../../../../../static/images/payment_smartphone.png';

class PaymentMethods extends Component {
    render() {
        return <div className='wrapperPaymentMethods'>
            <div className='wrapperIcon'>
                <Icon srcIcon={paymentMethodsImage} />
            </div>
        </div>
    }
}

export default PaymentMethods;
