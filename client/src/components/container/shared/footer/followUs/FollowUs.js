import React, { Component } from 'react';
import './followUs.css';

import Icon from '../../../../presentational/icon/Icon';
import faceIcon from '../../../../../static/icons/face_icon.svg';
import instIcon from '../../../../../static/icons/inst_icon.svg';

class FollowUs extends Component {
    render() {
        return <div className='wrapperFollowUs'>
            <p>Seguici sui nostri canali social.</p>
            <div className='wrapperIcon'>
                <a href='https://www.facebook.com/dvrsus.it/'><Icon srcIcon={faceIcon} /></a>
                <a href='https://www.instagram.com/dvrsus.it/'><Icon srcIcon={instIcon} /></a>
            </div>
        </div>
    }
}

export default FollowUs;
