import React from 'react';
import './footer.css'

import FollowUs from './followUs/FollowUs';
import LegalArea from './legalArea/LegalArea';
import PaymentMethods from './paymentMethods/PaymentMethods'

const Footer = () => <footer className='wrapperFooter'>
        <div className='footerFirstPart'>
            <FollowUs />
            <PaymentMethods />
        </div>
            <LegalArea />
    </footer>

export default Footer;
