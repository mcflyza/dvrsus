import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './removeFromWishlist.css';

class RemoveFromWishlist extends Component {

    constructor(props) {
        super();
        this.state = {
            wishlistRowId: props.wishRowId
        }

        this.onClick = this.onClick.bind(this);
    }

    removeFromWishlist(wishlistRowId) {
        axios
            .delete(`/api/user-data/wishlist/${wishlistRowId}`)
            .then(res => window.location.reload())
            .catch(err => console.log(err))
    }

    onClick(evt) {
        evt.preventDefault();
        this.removeFromWishlist(this.state.wishlistRowId);
    }

    render() {
        return (
            <div className='removeFromWishlistWrapper'>
                <button onClick={this.onClick} className='removeFromWishlistCTA'>
                    remove
                </button>
            </div>
        );
    }
};

RemoveFromWishlist.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(RemoveFromWishlist);