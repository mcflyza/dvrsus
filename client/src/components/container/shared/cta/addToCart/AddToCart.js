import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './addToCart.css';

class AddToCart extends Component {

    constructor(props) {
        super();

        this.state = {
            productId: props.productId
        }

        this.onClick = this.onClick.bind(this);
    }

    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.productId !== prevProps.productId) {
          this.setState({
            productId: this.props.productId
          });
        }
    }

    addWarningMessageIfNotExist() {
        let addToError = document.querySelector('.errorAddToCart');

        if(addToError  === null) {
            let isAuth = this.props.auth.isAuthenticated;
            let insertError = document.querySelector('.descriptionWrapper.product-card.label');
            let errorSpan = '';

            if(!isAuth) {
                errorSpan = '<span class="errorAddToCart">Dovresti loggarti per aggiungere al carrello</span>';
            } else if (this.props.error) {
                errorSpan = '<span class="errorAddToCart">Dovresti selezionare taglia e/o colore</span>';
            }
            insertError.insertAdjacentHTML('beforebegin', errorSpan)
        }
    }

    addToCart(userId, productId) {
        let addToCartParams = {
            'userId': `${userId}`,
            'productId': `${productId}`,
            'quantity': 1
        };

        axios
            .post(`/api/user-data/cart/add`, addToCartParams)
            .then()
            .catch(err => {
                console.log(err);
            })
    }

    removeErrorMessage(elem) {
        elem.parentNode.removeChild(elem);
    }

    onClick(evt) {
        evt.preventDefault();

        let isAuth = this.props.auth.isAuthenticated;
        if(!isAuth || this.props.error) {
            this.addWarningMessageIfNotExist();
        } else {
            let warningMessage = document.querySelector('.errorAddToCart');

            if (warningMessage != null) {
                this.removeErrorMessage(warningMessage);
            }

            //check if user have selected color and size
            const { user } = this.props.auth;
            this.state.productId !== '' && this.addToCart(user.id, this.state.productId);
        }
    }

    render() {
        return (
            <div className='addToCartWrapper'>
                <button onClick={this.onClick} className='addToCartCTA'>
                    AGGIUNGI AL CARRELLO
                </button>
            </div>
        );
    }
};

AddToCart.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(AddToCart);