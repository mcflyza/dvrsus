import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './removeFromCart.css';

class RemoveFromCart extends Component {

    constructor(props) {
        super();
        this.state = {
            cartRowId: props.cartRowId
        }

        this.onClick = this.onClick.bind(this);
    }

    removeFromCart(cartRowId) {
        axios
            .delete(`/api/user-data/cart/${cartRowId}`)
            .then(res => window.location.reload())
            .catch(err => console.log(err))
    }

    onClick(evt) {
        evt.preventDefault();
        this.removeFromCart(this.state.cartRowId);
    }

    render() {
        return (
            <div className='removeFromCartWrapper'>
                <button onClick={this.onClick} className='removeFromCartCTA'>
                    remove
                </button>
            </div>
        );
    }
};

RemoveFromCart.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(RemoveFromCart);