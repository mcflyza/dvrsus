import React from 'react';
import './disclaimer.css';

const Disclaimer = () => <p className='disclaimerMessage'> Spedizione gratuita in tutta Italia </p>;

export default Disclaimer;