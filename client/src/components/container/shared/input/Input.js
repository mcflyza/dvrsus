import React from 'react'
import './input.css'

class Input extends React.Component {
    constructor(props) {
        super(props);

        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    render() {
        return (
            <input 
                type={this.props.type} 
                placeholder={this.props.placeholder}  
                value={this.props.value} 
                onChange={this.handleChange} 
                required
            />
        );
    }
}

export default Input;