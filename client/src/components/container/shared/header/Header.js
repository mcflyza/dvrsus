import React from 'react';
import './header.css';

import Menu from '../menu/Menu';

const Header = () => 
    <header className='wrapperHeader'>
        <Menu />
    </header>

export default Header;
