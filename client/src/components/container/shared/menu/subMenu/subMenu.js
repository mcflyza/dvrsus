import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './subMenu.css';

import burgerMenuIcon from '../../../../../static/icons/burger_icon.svg'
import burgerOpenMenuIcon from '../../../../../static/icons/burger-open_icon.svg'

class subMenu extends Component {

    constructor(){
        super()
        this.state = {
            showMenu: false,
            showMenuItem: false,
            showMenuJeans: false
        }

        this.menuShowToggle = this.menuShowToggle.bind(this);
        this.menuShowToggleItem = this.menuShowToggleItem.bind(this);
        this.menuShowToggleJeans = this.menuShowToggleJeans.bind(this);
        this.menuShowToggleElement = this.menuShowToggleElement.bind(this);
    }
    
    menuShowToggle = () => {
        this.setState({showMenu: !this.state.showMenu})
        this.menuShowToggleElement();
    }

    menuShowToggleElement = () => {
        this.setState({
            showMenuItem: false,
            showMenuJeans: false
        })
    }

    menuShowToggleItem = () => {
        this.setState({
            showMenuItem: !this.state.showMenuItem,
            showMenuJeans: false
        })
    }

    menuShowToggleJeans = () => {
        this.setState({
            showMenuItem: false,
            showMenuJeans: !this.state.showMenuJeans
        })
    }

    removeShowMenu() {
        let subsMenu = document.querySelectorAll('.subMenu .innerSubMenuList');
        subsMenu.forEach(element => {
            element.classList.remove('showMenu');
        });
    }

    render() {

        let toPDPJeansData = {
            pathname: '/product-details-page',
            state: {
              productId: '5ceae7bcb42b7a62860307cd'
            }
        };

        let toPDPShirtBarredData = {
            pathname: '/product-details-page',
            state: {
              productId: '5ceae36ab42b7a62860307c7'
            }
        };

        let toPDPShirtVanGoghData = {
            pathname: '/product-details-page',
            state: {
              productId: '5ceae4b3b42b7a62860307c9'
            }
        };

        return (
            <div className='wrapper other'>
                <div className='icon' onClick={this.menuShowToggle}>
                    <img alt='icon' src={this.state.showMenu ? burgerOpenMenuIcon : burgerMenuIcon}></img>
                </div>
                <ul className={this.state.showMenu ? 'showMenu': 'hideMenu'}>
                    <li className='menuItem tShirt'>
                        <section className='listItemsSubMenu'>
                            <h3 onClick={this.menuShowToggleItem}><strong>T-Shirt</strong></h3>
                            <ul className={this.state.showMenuItem ? 't-shirt innerSubMenuList showMenu': 't-shirt innerSubMenuList hideMenu'}>
                                <li onClick={this.menuShowToggle}>
                                    <Link to={toPDPShirtBarredData}>Barred</Link>
                                </li>
                                <li onClick={this.menuShowToggle}>
                                    <Link to={toPDPShirtVanGoghData}>Van-Gogh</Link>
                                </li>
                            </ul>
                        </section>
                    </li>
                    <li className='menuItem jeans'>
                        <section className='listItemsSubMenu'>
                            <h3 onClick={this.menuShowToggleJeans}><strong>Jeans</strong></h3>
                            <ul className={this.state.showMenuJeans ? 'showMenu innerSubMenuList jeans': 'hideMenu innerSubMenuList jeans'}>
                                <li onClick={this.menuShowToggle}>
                                    <Link to={toPDPJeansData}>Diversus As Us Jeans</Link>
                                </li>
                            </ul>
                        </section>
                    </li>
                    <li className='menuItem contacts' onClick={this.menuShowToggle}>
                        <h3><Link to='/contacts'><strong>Contatti</strong></Link></h3>
                    </li>
                </ul>
            </div>
        );
    }
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default (connect(mapStateToProps)(subMenu));
