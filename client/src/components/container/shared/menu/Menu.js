import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './menu.css';

import Logo from '../logo/Logo';
import SubMenu from './subMenu/subMenu';

import Icon from '../../../presentational/icon/Icon'
import cartIcon from '../../../../static/icons/cart_icon.svg'
import favoriteIcon from '../../../../static/icons/heart_icon.svg'
import accountIcon from '../../../../static/icons/account_icon.svg'

class Menu extends Component {

  render() {
    return (
      	<div className='wrapperMenu'>
        	<nav className='navMenu'> 
                <ul className='menuListItem'>
					<div className='menuListItem wrapperMenuLogo'>
						<li className='menuItem logo'>
							<Link to='/'>
								<Logo />
							</Link>     
						</li>
					</div>
					<div className='menuListItem wrapperMenuIcons'>
						<ul className='menuListItem wrapperMenuIconsInner'>
							<div className='wrapperMenuIconsRight'>
								<li className='menuItem wishlist'>
									<Link to='/wishlist'>
										<Icon srcIcon={favoriteIcon} />
									</Link>  
								</li>
								<li className='menuItem account'>
									<Link to='/account'>
										<Icon srcIcon={accountIcon} />
									</Link>  
								</li>
								<li className='menuItem cart'>
									<Link to='/cart'>
										<Icon srcIcon={cartIcon} />
									</Link>  
								</li>
							</div>
							<li className='menuItem subMenu'>
								<SubMenu />
							</li>
						</ul>
                  </div>
                </ul>
            </nav>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default (connect(mapStateToProps)(Menu));
