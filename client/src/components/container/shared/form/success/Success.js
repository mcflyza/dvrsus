import React from 'react'
import './success.css'

const Success = () => 
    <div className='success'>
        <p>Complimenti! Ti sei iscritto con <strong>SUCCESSO</strong></p>
    </div>

export default Success;