import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginUser } from '../../../../../redux/actions/authActions';
import { Redirect } from 'react-router-dom'

import './loginForm.css'

import Input from '../../input/Input'

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'email',
            psw: 'psw',
            rdrct: false
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePsw = this.handleChangePsw.bind(this);
    }

    handleChangePsw(event) {
        this.setState({
            psw: event.target.value
        });
    }
    
    handleChangeEmail(event) {
        this.setState({
            email: event.target.value
        });
    }

    handleChange(event) {
        let input = event.target.placeholder;
        switch (input) {
            case 'Password': this.handleChangePsw(event); break;
            case 'E-mail': this.handleChangeEmail(event); break;
            default: input = null;
        }
    }

    setRedirect = () => {
        this.setState({
          redirect: true
        })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/'/>
        }
    }

    onSubmit = evt => {
        evt.preventDefault();
        
        const userData = {
            email: this.state.email,
            psw: this.state.psw
        };

        this.props.loginUser(userData);
        this.setRedirect();
    };

    render() {
        return (
            <div id='loginForm'>
                {this.renderRedirect()}
                <form className={this.props.formVisibility ? 'showForm' : 'hideForm'} onSubmit={this.onSubmit} onChange={this.handleChange}>
                    <Input className='loginFormInput' type="email" placeholder='E-mail'/>
                    <Input className='loginFormInput' type='password' placeholder='Password'/>
                    <Input className='loginFormInput' type='submit' value='ACCEDI'/>
                </form>
            </div>
        );
    }
}

LoginForm.propTypes = {
    loginUser: PropTypes.func.isRequired
};

export default connect(
    null,
    { loginUser }
)(LoginForm);