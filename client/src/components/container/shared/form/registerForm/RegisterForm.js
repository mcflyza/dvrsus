import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { registerUser } from '../../../../../redux/actions/authActions';
import './registerForm.css';

import Input from '../../input/Input';
import Success from '../success/Success';

class RegisterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: 'firstname',
            lastname: 'lastname',
            email: 'email',
            psw: 'psw',
            success: false
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeFirstname = this.handleChangeFirstname.bind(this);
        this.handleChangeLastname = this.handleChangeLastname.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePsw = this.handleChangePsw.bind(this);
    }

    handleChangeFirstname(event) {
        this.setState({
            firstname: event.target.value
        });
    }

    handleChangeLastname(event) {
        this.setState({
            lastname: event.target.value
        });
    }

    handleChangePsw(event) {
        this.setState({
            psw: event.target.value
        });
    }
    
    handleChangeEmail(event) {
        this.setState({
            email: event.target.value
        });
    }

    handleChange(event) {
        let input = event.target.placeholder;
        switch (input) {
            case 'Nome': this.handleChangeFirstname(event); break;
            case 'Cognome': this.handleChangeLastname(event); break;
            case 'Password': this.handleChangePsw(event); break;
            case 'E-mail': this.handleChangeEmail(event); break;
            default: input = null;
        }
    }

    onSubmit = evt => {
        evt.preventDefault();

        const userData = {
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            email: this.state.email,
            psw: this.state.psw
        };
        
        this.props.registerUser(userData);
        this.setState({success: true});
    };

    render() {
        return this.state.success ? <Success/> : 
            <div id='registerForm'>
                <form className={this.props.formVisibility ? 'showForm' : 'hideForm'}  onSubmit={this.onSubmit} onChange={this.handleChange}>
                    <Input className='registerFormInput' type="text" placeholder='Nome'/>
                    <Input className='registerFormInput' type="text" placeholder='Cognome'/>
                    <Input className='registerFormInput' type="email" placeholder='E-mail'/>
                    <Input className='registerFormInput' type='password' placeholder='Password'/>
                    <Input className='registerFormInput' type='submit' value='REGISTRATI'/>
                </form>
            </div>
    }
}

RegisterForm.propTypes = {
    registerUser: PropTypes.func.isRequired
};

export default connect(
    null,
    { registerUser }
)(RegisterForm);