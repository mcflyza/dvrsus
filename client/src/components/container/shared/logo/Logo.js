import React, { Component } from 'react';
import './logo.css'

import Icon from '../../../presentational/icon/Icon'
import srcLogo from '../../../../static/icons/logo.svg'

class Logo extends Component {
    render() {
        return <div className='wrapperLogo'>
            <Icon srcIcon={srcLogo}/>
        </div>
    }
}

export default Logo;
