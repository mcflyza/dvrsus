import React, { Component } from 'react';
import './auth.css'

import Register from '../../auth/Register';
import Login from '../../auth/Login';
import ToggleLoginRegister from './toggleLoginRegister/ToggleLoginRegister';

class Auth extends Component {
    constructor() {
        super();
        this.state = {
            toggleLoginRegister: false
        };

        this.toggleLoginRegister = this.toggleLoginRegister.bind(this);
    }

    toggleLoginRegister = () => {
        this.setState({toggleLoginRegister: !this.state.toggleLoginRegister})
    }

    render() {
        const MESSAGE_FOR_REGISTRATION = 'Sei già registrato?';
        const MESSAGE_FOR_LOGIN = 'Sei un nuovo utente?';

        const boolLoginRegister = this.state.toggleLoginRegister;

        return <div className='wrapperAuth wrapperInnerMain'>
            <div className='innerWrapperAuth'>
                <div className='wrapperAuthLogin'>
                    <Login formVisibility={!boolLoginRegister}/>
                    {boolLoginRegister && <ToggleLoginRegister onClick={this.toggleLoginRegister}/>}
                </div>
                <span className='middleMessageAuth'>
                    {boolLoginRegister ? MESSAGE_FOR_REGISTRATION : MESSAGE_FOR_LOGIN}
                </span>
                <div className='wrapperAuthRegistration'>
                    <Register formVisibility={boolLoginRegister}/> 
                    {!boolLoginRegister && <ToggleLoginRegister onClick={this.toggleLoginRegister}/>}
                </div>
            </div>

        </div>
    }
}

export default Auth;
