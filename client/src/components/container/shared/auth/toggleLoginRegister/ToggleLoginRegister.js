import React, {Component} from 'react';
import './toggleLoginRegister.css';

class ToggleLoginRegister extends Component {
    render() {
        return (
            <div className='wrapperToggleLoginRegister' onClick={this.props.onClick}>
                <div>CLICCA QUI</div>
            </div>
        );
    }
};

export default ToggleLoginRegister;