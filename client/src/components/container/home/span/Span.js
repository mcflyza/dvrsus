import React from 'react';
import './span.css'

const Span = (props) => (
    <div className={props.mobile === 'true' ? 'onlyMobile spanWrapper' : 'spanWrapper'}>
        <span>DVRSUS AS US</span>
    </div>
);
  
export default Span;