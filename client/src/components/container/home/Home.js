import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './home.css';

import Span from './span/Span';

class Home extends Component {
    constructor() {
      super();
      this.state = {}
    }

    render() {

        let toPDPJeansData = {
          pathname: '/product-details-page',
          state: {
            productId: '5ceae7bcb42b7a62860307cd'
          }
        };

        let toPDPShirtBarredData = {
          pathname: '/product-details-page',
          state: {
            productId: '5ceae36ab42b7a62860307c7'
          }
        };

        let toPDPShirtVanGoghData = {
          pathname: '/product-details-page',
          state: {
            productId: '5ceae4b3b42b7a62860307c9'
          }
        };

        return (
          <div className='wrapperHome'>
            <div className='previewProduct barredPreview'>
              <section className='sectionPreviewWrapper barredPreview'>
                <div className='sectionPreviewWrapperInner alignRight'>
                  <h2>T-Shirt</h2>
                  <h5>Barred</h5>
                  <div className='interactiveWrapper'>
                    <span>(disponibile in più colori)</span>
                    <Link to={toPDPShirtBarredData}>
                      <button>VISUALIZZA PRODOTTO</button>
                    </Link>
                  </div>
                </div>
              </section>
              <section className='sectionPreviewWrapper forDesktop jeansBlue'>
                <div className='sectionPreviewWrapperInner alignLeft'>
                  <h2>Jeans</h2>
                  <h5>Istanbul</h5>
                  <div className='interactiveWrapper'>
                    <span>(disponibile in più colori)</span>
                    <Link to={toPDPJeansData}>
                      <button>VISUALIZZA PRODOTTO</button>
                    </Link>
                  </div>
                </div>
              </section>
            </div>
            <Span />
            <div className='previewProduct vanGoghPreview'>
			        <section className='sectionPreviewWrapper vanGoghPreview'>
                <div className='sectionPreviewWrapperInner alignRight'>
                  <h2>T-Shirt</h2>
                  <h5>Van Gogh</h5>
                  <div className='interactiveWrapper'>
                    <span>(disponibile in più colori)</span>
                    <Link to={toPDPShirtVanGoghData}>
                      <button>VISUALIZZA PRODOTTO</button>
                    </Link>
                  </div>
                </div>
              </section>
              <section className='sectionPreviewWrapper jeansBlack forDesktop'>
                <div className='sectionPreviewWrapperInner alignLeft'>
                  <h2>Jeans</h2>
                  <h5>San Diego</h5>
                  <div className='interactiveWrapper forDesktop'>
                    <span>(disponibile in più colori)</span>
                    <Link to={toPDPJeansData}>
                      <button>VISUALIZZA PRODOTTO</button>
                    </Link>
                  </div>
                </div>
              </section>
            </div>
            <Span mobile='true'/>
            <div className='previewProduct jeansPreview onlyMobile'>
			        <section className='sectionPreviewWrapper jeansPreview'>
                <div className='sectionPreviewWrapperInner'>
                  <h2>Jeans</h2>
                  <h5>Dvrsus as Us</h5>
                  <div className='interactiveWrapper'>
                    <span>(disponibile in più colori)</span>
                    <Link to={toPDPJeansData}>
                      <button>VISUALIZZA PRODOTTO</button>
                    </Link>
                  </div>
                </div>
              </section>
            </div>
          </div>
        );
    }
}

export default Home;