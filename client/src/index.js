import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
//import Provider to connect your app component to the store,  by wrapping your BrowserRouter
import { Provider } from 'react-redux';
//import the store that will be your store for your app .
import store from './redux/store';
import * as serviceWorker from './serviceWorker';

import DVRSUS from './components/container/DVRSUS/DVRSUS';

import './index.css';

const StartApp = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <DVRSUS />
            </BrowserRouter>
        </Provider> 
    )
}

ReactDOM.render(<StartApp/>, document.getElementById('rootDvrsus'));
serviceWorker.unregister();
